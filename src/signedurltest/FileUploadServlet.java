package signedurltest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.appengine.api.files.GSFileOptions.GSFileOptionsBuilder;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.Transform;
//import java.nio.channels.Channels;
//------------------------------------------
/*データ　タイプ　ライブラリ*/
//import java.util.Date;
//import java.util.HashSet;
//import org.apache.commons.io.output.DeferredFileOutputStream;
//import org.apache.commons.fileupload.FileItemIterator;
//import org.apache.commons.fileupload.FileItemStream;
//import org.apache.commons.fileupload.servlet.ServletFileUpload;
//import org.apache.commons.fileupload.disk.*;
//import org.apache.commons.io.output.*;
//SignedURL が500文字を超えてしまう為にこれが必要


@SuppressWarnings("serial")
public final class FileUploadServlet extends HttpServlet {
//    private static final String BASE_URL		= "https://storage.googleapis.com"; こっちでないとだめ
//	public static final String GOOGLE_STORAGE = "storage.cloud.google.com/"; このURLは使わないこと
	public static final String GOOGLE_STORAGE = "https://storage.googleapis.com/";


	private String bucketName = "uoz_test";
	private String folderName = "default";

	private String aclString = "project_private";

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
	        /* 返されるデータの形式をapplication/jsonに変更する */
	        resp.setContentType("application/json; charset=UTF-8");

		  	/* 入力項目を取得する */
//	        String mode 		= req.getParameter("mode");
//			String email 		= req.getParameter("email");
//			String password 	= req.getParameter("password");
			   /* ログイン画面を表示する */
		    RequestDispatcher rd = getServletContext().getRequestDispatcher(
		            "/fileselect");
		    rd.forward(req, resp);
    }

	@SuppressWarnings("deprecation")
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");

		ServletFileUpload upload = new ServletFileUpload();
        try {
            FileItemIterator fileIte = upload.getItemIterator(req);

			Long cardId=null;
			String cardid_str=null;
			String canvasid_str=null;
			byte[] byteArray=null;
			String contentType="";
			String className_str="";

			String fileName="file";

	        while (fileIte.hasNext()) {
		        FileItemStream item = fileIte.next();
		        String fieldName = item.getFieldName();
		        InputStream fieldValue = item.openStream();

	            if ("card_id".equals(fieldName)) {
	            	//キャンバスとカードとの関係を管理するためのCPhotoデータ作成用
	            	cardid_str = Streams.asString(fieldValue);
	                continue;
	            }
	            else if ("content_type".equals(fieldName)) {
	            	//ファイル種別　jpg,png,gif
	            	contentType = Streams.asString(fieldValue);
	                continue;
	            }
	            else if ("canvas_id".equals(fieldName)) {
	            	//バケット名にする
	            	canvasid_str = Streams.asString(fieldValue);
	                continue;
	            }
	            else if("class_name".equals(fieldName)){
	            	//cardか、category_boxかで、保存先を変更する
	            	className_str = Streams.asString(fieldValue);
	            	continue;
	            }
	            else if ("datafile".equals(fieldName)) {
	            	contentType = item.getContentType();
					ByteArrayOutputStream out = new ByteArrayOutputStream();
	                Streams.copy(fieldValue, out, true);//outへfieldValueをコピー
	                byteArray = out.toByteArray();

	                fileName = item.getName(); //ファイル名

	                continue;
	            }
	            else if ("folder_name".equals(fieldName)){
	            	//folderName = fieldValue.toString();
	            	folderName = Streams.asString(fieldValue);

	            }
	        }
	        //画像サイズを一律に縮小
	        ImagesService imagesService = ImagesServiceFactory.getImagesService();
	        Image oldImage = ImagesServiceFactory.makeImage(byteArray);
	        Transform resize = ImagesServiceFactory.makeResize(300, 300);
	        Image newImage = imagesService.applyTransform(resize, oldImage);
	        byte[] newImageData = newImage.getImageData();

	        //画像属性を取得してPhotoPathを生成
	        /*
	        String gsFilename = cardid_str;
	        if(contentType.contains("png")){
	        	gsFilename = (gsFilename + ".png");
			}else if (contentType.contains("jpeg")){
				gsFilename = (gsFilename + ".jpg");
			}else if(contentType.contains("gif")){
				gsFilename = (gsFilename + ".gif");
			}
			*/

	        //ファイル名だけを取得
	        String gsFilename =  getFileNameOnly(fileName).toLowerCase();


	        FileService fileService = FileServiceFactory.getFileService();
	        GSFileOptionsBuilder optionsBuilder = new GSFileOptionsBuilder()
//	                .setBucket(canvasid_str)
	                .setBucket(bucketName)
	                //.setKey(gsFilename)
	                .setKey(folderName +  "/"+ gsFilename)
	                //.setMimeType("binary/octet-stream")
	                .setMimeType("image/jpeg")
	                .setAcl(aclString);
	        AppEngineFile file = fileService.createNewGSFile(optionsBuilder.build());
	        FileWriteChannel writeChannel = fileService.openWriteChannel(file, true);

//	        ByteBuffer buffer = ByteBuffer.wrap(byteArray);
	        ByteBuffer buffer = ByteBuffer.wrap(newImageData);
	        writeChannel.write(buffer);
	        writeChannel.closeFinally();

	        //SignedURLの生成



//	        GcsAppIdentityServiceUrlSigner signedUrl = new GcsAppIdentityServiceUrlSigner();
	        String httpVerb		= "GET";
	//        String bucket		= "cloudstormfiles";
//	        String fileName		= gsFilename;
//	        String singnedUrl_str = signedUrl.getSignedUrl(httpVerb, bucket, fileName);
//	        String singnedUrl_str = signedUrl.getSignedUrl(httpVerb, bucket, fileName);

//	        Text singnedUrl_txt = new Text(singnedUrl_str);//GAEの制約のためTextへ変換
	        //cardへのPhotoPathを設定する(GOOGLE_STORAGEは入れない）
//	        cardId = Long.parseLong(cardid_str);
//	        CCanvasHelper canvasHelper = new CCanvasHelper();
//	        canvasHelper.setPhotoPathToCard(cardId, canvasid_str+"/"+gsFilename);

//	        Text singnedUrl_txt = new Text("cloudstormfiles/"+gsFilename);//GAEの制約のためTextへ変換
/*	        if(className_str.equals("card")){
		        canvasHelper.setPhotoPathToCard(cardId, "cloudstormfiles/"+gsFilename);
	        }else if(className_str.equals("category_box")){
		        canvasHelper.setPhotoPathToCategory(cardId, "cloudstormfiles/"+gsFilename);

	        }
	        */
//	        canvasHelper.setPhotoPathToCard(cardId, singnedUrl_txt);
//	        canvasHelper.setPhotoPathToCard(cardId, singnedUrl_txt);
	        //クライアント側にはphotoPathが返れば良い
	        //https://storage.cloud.google.com/<bucket>/<object>. が標準
			resp.setContentType("application/json; charset=UTF-8");
//	        String photoPath = GOOGLE_STORAGE + canvasid_str+"/"+gsFilename;
	        String photoPath = GOOGLE_STORAGE + bucketName + "/"+ folderName + "/"+ gsFilename;


			String signedURL = null;

			System.out.println(photoPath);

			GcsAppIdentityServiceUrlSigner signer = new GcsAppIdentityServiceUrlSigner();
			try {
				signedURL = signer.getSignedUrl(httpVerb, bucketName, folderName + "/"+ gsFilename);


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(signedURL != null){
				resp.setContentType("text/html");

				PrintWriter out = resp.getWriter();
				out.println("<img src=" + signedURL + "><br />");
				out.println("<p>" + signedURL + "</p>");

				System.out.println(signedURL);
			}

			/*
	        //アレイかクラスでないと、JSONで送信できないので。。。とりあえずアレイにする
			ArrayList<String> retmsg = new ArrayList<String>();
			retmsg.add(photoPath);
//			retmsg.add(singnedUrl_str);

			String test = JSON.encode(retmsg);
			System.out.println(test);//for debug
			JSON.encode(retmsg, resp.getOutputStream());
			*/

        } catch (FileUploadException e) {
	            throw new ServletException(e);
	    }
/*        catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/
    }

	public String getFileNameOnly (String f){

		String result = "";

	      String[] parts = f.split("/");

	      for(int i=0;i<parts.length; i++){
	        System.out.println("[" + i + "] " + parts[i]);
	      }

	      String t = parts[parts.length-1];

	      System.out.println(t);
	      int idx = t.indexOf("?");
	      if(-1 < idx ){
	  		result = t.substring(0, t.indexOf("?"));
	      }
	      else{
	    	  result = t;
	      }
		return 	result;



	}
}


