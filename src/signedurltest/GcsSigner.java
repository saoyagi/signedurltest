package signedurltest;
// Copyright 2012 Google Inc. All Rights Reserved.
// Usage: java GcsSigner path/to/key "key password" "String to sign"

import org.apache.commons.codec.binary.Base64;  // From Apache Commons Codec.
                                                // http://commons.apache.org/codec/

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.UnrecoverableKeyException;

public class GcsSigner {
  public static String sign(String[] args) throws Exception {
    if (args.length != 3) {
      System.err.println("Usage: java GcsSigner path/to/key \"key password\" \"String to sign\"");
      System.exit(-1);
    }

    String keyFile = args[0];
    String keyPassword = args[1];
    String stringToSign = args[2];

    PrivateKey key = loadKeyFromPkcs12(keyFile, keyPassword.toCharArray());
    String signature = signData(key, stringToSign);
    System.out.println(signature);
    return signature;

  }

  private static PrivateKey loadKeyFromPkcs12(String filename, char[] password) throws Exception {
    FileInputStream fis = new FileInputStream(filename);
    KeyStore ks = KeyStore.getInstance("PKCS12");
    try {
      ks.load(fis, password);
    } catch (IOException e) {
      if (e.getCause() != null && e.getCause() instanceof UnrecoverableKeyException) {
        System.err.println("Incorrect password");
      }
      throw e;
    }
    return (PrivateKey)ks.getKey("privatekey", password);
  }


  private static String signData(PrivateKey key, String data) throws Exception {
    Signature signer = Signature.getInstance("SHA256withRSA");
    signer.initSign(key);
    signer.update(data.getBytes("UTF-8"));
    byte[] rawSignature = signer.sign();
    String encodedSignature = new String(Base64.encodeBase64(rawSignature, false), "UTF-8");
    return encodedSignature;
  }
}