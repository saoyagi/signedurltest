package signedurltest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ACLTest2Servlet extends HttpServlet {


	private String bucket = "uoz_test";
	//private String objectName = "2.jpg";
	//private String objectName = "004.JPG";
	private String objectName = "005.JPG";

	private String baseURL = "http://storage.googleapis.com/" +  bucket + "/" + objectName;

	private String HTTP_Verb = "GET";
	public  void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		String signedURL = null;

		System.out.println(baseURL);

		GcsAppIdentityServiceUrlSigner signer = new GcsAppIdentityServiceUrlSigner();
		try {
			signedURL = signer.getSignedUrl(HTTP_Verb, bucket, objectName);


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(signedURL != null){
			resp.setContentType("text/html");

			PrintWriter out = resp.getWriter();
			out.println("<img src=" + signedURL + "><br />");
			out.println("<p>" + signedURL + "</p>");

			System.out.println(signedURL);
		}


	}
}
