package signedurltest;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/*
 * google cloud storage
 * signed URL機能をテストするためのクラス
 * 2014 2/11 saizo aoyagi
 */


//see https://developers.google.com/storage/docs/accesscontrol#Signed-URLs



public class ACLTestServlet extends HttpServlet {

	//private final String mode = "local";
	private final String mode = "gae";
	/*
	private String pathToKey = "./file/f916e956bf9cb2357037f707b8b9da6fa9607074-privatekey.p12";
	private String passwordOfKey = "notasecret";
	*/

	private String pathToKey = "file/b220d104b80d938a91f4cbff0b5f84561bc3e8f0-privatekey.p12";
	private String passwordOfKey = "notasecret";

	private String bucket = "uoz_test";
	private String objectName = "2.jpg";

	private String baseURL = "http://storage.googleapis.com/" +  bucket + "/" + objectName;

	private String HTTP_Verb = "GET";


	/*
	private String Expiration = null;
	private String Canonicalized_Resource ="/" +  bucket + "/" + objectName;

	private String GoogleAccessStorageId = "1079542841093-0b00eise8hgrlglqv45sspvrgk15bonq@developer.gserviceaccount.com";
	*/

	private HttpURLConnection connection = null;

	public  void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		System.out.println(baseURL);
		resp.setContentType("image/jpeg");
		ServletOutputStream out = resp.getOutputStream();
		BufferedInputStream bufferedInputStream = null;
		try {
			bufferedInputStream = getBufferedInputStream();
			int iBody = 0;
			while ((iBody = bufferedInputStream.read()) != -1) {
				out.write(iBody);
			}

			if (bufferedInputStream != null) {
				try{
					bufferedInputStream.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		if (out != null) {
			try{
				out.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}

		if (connection != null){
			connection.disconnect();
		}

	}

	@SuppressWarnings("unused")
	private BufferedInputStream getBufferedInputStream () throws Exception{
		BufferedInputStream bufferedInputStream = null;
		if(mode == "local"){
			String realPath = this.getServletContext().getRealPath("./images/test.jpg");
			bufferedInputStream = new BufferedInputStream(new FileInputStream(realPath));
		}else{
			GcsAppIdentityServiceUrlSigner signer = new GcsAppIdentityServiceUrlSigner();
			String signedURL;

			signedURL = signer.getSignedUrl(HTTP_Verb, bucket, objectName);
			URL url = new URL(signedURL);

			connection = (HttpURLConnection) url.openConnection();
			int responseCode = connection.getResponseCode();
			if (responseCode != HttpURLConnection.HTTP_OK) {
			    return null;
			}

			int length = connection.getContentLength();
			if (length <= 0) {
			    return null;
			}

			bufferedInputStream = new BufferedInputStream(connection.getInputStream());



		}
		return bufferedInputStream;
	}

}
